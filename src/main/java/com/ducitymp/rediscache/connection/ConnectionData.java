package com.ducitymp.rediscache.connection;

public class ConnectionData {

    private final String host;
    private final int port;
    private final String password;

    public ConnectionData(String host, int port) {
        this(host, port, null);
    }

    public ConnectionData(String host, int port, String password) {
        this.host = host;
        this.port = port;
        this.password = password;

    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getPassword() {
        return password;
    }

}
