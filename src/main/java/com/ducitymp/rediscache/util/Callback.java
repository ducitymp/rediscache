package com.ducitymp.rediscache.util;

@FunctionalInterface
public interface Callback<T> {

    void onResult(T t);

}