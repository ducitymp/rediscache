package com.ducitymp.rediscache.util;

@FunctionalInterface
public interface Consumer {

    void accept();

}