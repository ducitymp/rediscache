package com.ducitymp.rediscache;

import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.connection.ConnectionData;
import com.ducitymp.rediscache.exception.NoHostFoundException;
import com.google.common.collect.Maps;

import java.util.Map;

public class RedisCacheBuilder {

    private ConnectionData connectionData = null;
    private Map<String, String> config = Maps.newHashMap();

    public RedisCacheBuilder(ConnectionData connectionData) {
        this.connectionData = connectionData;
    }

    public RedisCacheBuilder setConnectionData(ConnectionData connectionData) {
        this.connectionData = connectionData;
        return this;
    }

    public RedisCacheBuilder addConfigOption(String key, String value) {
        config.put(key, value);
        return this;
    }

    public RedisCache buildCache() {
        try {
            if (connectionData == null) {
                throw new NoHostFoundException();
            }

            return new RedisCache(connectionData, config);
        } catch (NoHostFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
