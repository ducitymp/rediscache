package com.ducitymp.rediscache.exception;

public class NoHostFoundException extends Exception {

    public NoHostFoundException() {
        super("No hosts were supplied in your builder");
    }

}
