package com.ducitymp.rediscache.cache.types.async.scheduler;

import com.ducitymp.rediscache.cache.scheduler.CacheScheduler;
import com.ducitymp.rediscache.cache.types.async.ASyncRedisCache;
import com.ducitymp.rediscache.util.Consumer;

public class ASyncCacheScheduler extends CacheScheduler<ASyncRedisCache> {

    public ASyncCacheScheduler(ASyncRedisCache redisCache) {
        super(redisCache);
    }

    public void setLater(String primaryKey, String key, Object object, int runAfterSeconds, Consumer consumer) {
        scheduleLater(e -> getRedisCache().set(primaryKey, key, object), runAfterSeconds, consumer);
    }

    public void setLater(String primaryKey, String key, Object object, int runAfterSeconds) {
        setLater(primaryKey, key, object, runAfterSeconds, null);
    }

    public void setTimer(String primaryKey, String key, Object object, int delay, Consumer consumer) {
        scheduleTimer(e -> getRedisCache().set(primaryKey, key, object), delay, consumer);
    }

    public void setTimer(String primaryKey, String key, Object object, int delay) {
       setTimer(primaryKey, key, object, delay, null);
    }

    public void removeLater(String primaryKey, String key, int runAfterSeconds, Consumer consumer) {
        scheduleLater(e -> getRedisCache().remove(primaryKey, key), runAfterSeconds, consumer);
    }

    public void removeLater(String primaryKey, String key, int runAfterSeconds) {
       removeLater(primaryKey, key, runAfterSeconds, null);
    }

    public void removeTimer(String primaryKey, String key, int delay, Consumer consumer) {
        scheduleTimer(e -> getRedisCache().remove(primaryKey, key), delay, consumer);
    }

    public void removeTimer(String primaryKey, String key, int delay) {
        removeTimer(primaryKey, key, delay, null);
    }

}
