package com.ducitymp.rediscache.cache.types.async;

import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.cache.types.async.scheduler.ASyncCacheScheduler;
import com.ducitymp.rediscache.connection.ConnectionData;
import com.ducitymp.rediscache.util.Callback;
import com.ducitymp.rediscache.util.Reflections;
import com.google.common.primitives.Primitives;
import com.google.gson.Gson;
import org.apache.commons.lang3.Validate;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.function.Supplier;

public class ASyncRedisCache extends RedisCache {

    public ASyncRedisCache(ConnectionData connectionData, Map<String, String> config) {
        super(connectionData, config);
    }

    public void set(String primaryKey, String key, Object object) {
        new Thread(() -> {
            Jedis jedis = generateConnection();

            getConfig().forEach(jedis::configSet);

            Gson gson = new Gson();
            String objectString = gson.toJson(object);

            jedis.hset(primaryKey, key, objectString);
            jedis.close();
        }).run();
    }

    public void remove(String primaryKey, String key) {
        new Thread(() -> {
            Jedis jedis = generateConnection();
            jedis.hdel(primaryKey, key);
            jedis.close();
        }).run();
    }

    public <T> void get(String primaryKey, String key, Class<T> objectClass, Callback<T> callback) {
        new Thread(() -> {
            Jedis jedis = generateConnection();

            String dataString = jedis.hget(primaryKey, key);
            Gson gson = new Gson();

            callback.onResult(as(dataString == null ? null : gson.fromJson(dataString, objectClass), objectClass));

            jedis.close();
        }).run();
    }

    @Override
    public ASyncCacheScheduler schedule() {
        return new ASyncCacheScheduler(this);
    }

}

