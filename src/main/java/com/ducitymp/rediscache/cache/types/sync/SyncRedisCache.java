package com.ducitymp.rediscache.cache.types.sync;

import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.cache.types.sync.scheduler.SyncCacheScheduler;
import com.ducitymp.rediscache.connection.ConnectionData;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.function.Supplier;

public class SyncRedisCache extends RedisCache {

    public SyncRedisCache(ConnectionData connectionData, Map<String, String> config) {
        super(connectionData, config);
    }

    public void set(String primaryKey, String key, Object object) {
        Jedis jedis = generateConnection();

        getConfig().forEach(jedis::configSet);

        Gson gson = new Gson();
        String objectString = gson.toJson(object);

        jedis.hset(primaryKey, key, objectString);
        jedis.close();
    }

    public void remove(String primaryKey, String key) {
        Jedis jedis = generateConnection();
        jedis.hdel(primaryKey, key);
        jedis.close();
    }

    public <T> T get(String primaryKey, String key, Class<T> objectClass) {
        Jedis jedis = generateConnection();

        String dataString = jedis.hget(primaryKey, key);
        Gson gson = new Gson();

        T data = as(dataString == null ? null : gson.fromJson(dataString, objectClass), objectClass);

        jedis.close();

        return data;
    }

    @Override
    public SyncCacheScheduler schedule() {
        return new SyncCacheScheduler(this);
    }

}
