package com.ducitymp.rediscache.cache;

import com.ducitymp.rediscache.cache.scheduler.CacheScheduler;
import com.ducitymp.rediscache.cache.types.async.ASyncRedisCache;
import com.ducitymp.rediscache.cache.types.sync.SyncRedisCache;
import com.ducitymp.rediscache.connection.ConnectionData;
import com.ducitymp.rediscache.util.Reflections;
import com.google.common.primitives.Primitives;
import org.apache.commons.lang3.Validate;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.JedisShardInfo;

import java.util.Map;

public class RedisCache extends JedisPubSub {

    private final ConnectionData connectionData;
    private final Map<String, String> config;

    public RedisCache(ConnectionData connectionData, Map<String, String> config) {
        this.connectionData = connectionData;
        this.config = config;
    }

    public ASyncRedisCache async() {
        return new ASyncRedisCache(connectionData, config);
    }

    public SyncRedisCache sync() {
        return new SyncRedisCache(connectionData, config);
    }

    protected <T extends CacheScheduler> T schedule() {
        return null;
    }

    protected Map<String, String> getConfig() {
        return config;
    }


    protected Jedis generateConnection() {
        JedisShardInfo info = new JedisShardInfo(connectionData.getHost(), connectionData.getPort());

        if (connectionData.getPassword() != null) {
            info.setPassword(connectionData.getPassword());
        }

        return new Jedis(info);
    }

    protected <T> T as(Object object, Class<T> c) {
        Validate.notNull(c, "Cannot cast to null");

        if (Primitives.isWrapperType(c) || Primitives.isWrapperType(Primitives.wrap(c))) {
            if (object == null) {
                return Reflections.defaultPrimitiveValue(c);
            }

            return Primitives.wrap(c).cast(object);
        }

        if (object == null) {
            return null;
        }

        if (c == String.class) {
            return (T) String.valueOf(object);
        }

        if (c.isInstance(object)) {
            return c.cast(object);
        }

        throw new ClassCastException("Unable to cast object");
    }
}
