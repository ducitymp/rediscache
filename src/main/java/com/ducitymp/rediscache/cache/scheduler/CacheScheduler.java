package com.ducitymp.rediscache.cache.scheduler;

import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.util.Consumer;

import javax.swing.*;
import java.awt.event.ActionListener;

public abstract class CacheScheduler<T extends RedisCache> {

    private final T redisCache;

    public CacheScheduler(T redisCache) {
        this.redisCache = redisCache;
    }

    protected void scheduleLater(ActionListener actionListener, int runAfterSeconds, Consumer consumer) {
        Timer timer = new Timer(runAfterSeconds * 1000, actionListener);

        timer.addActionListener(e -> {
            if(consumer != null) {
                consumer.accept();
            }
        });

        timer.setRepeats(false);
        timer.start();
    }

    protected void scheduleTimer(ActionListener actionListener, int delayInSeconds, Consumer consumer) {
        Timer timer = new Timer(delayInSeconds * 1000, actionListener);

        timer.addActionListener(e -> {
            if(consumer != null) {
                consumer.accept();
            }
        });

        timer.setRepeats(true);
        timer.start();
    }

    protected T getRedisCache() {
        return redisCache;
    }
}
