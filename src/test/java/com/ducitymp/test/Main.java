package com.ducitymp.test;

import com.ducitymp.rediscache.RedisCacheBuilder;
import com.ducitymp.rediscache.cache.RedisCache;
import com.ducitymp.rediscache.connection.ConnectionData;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final String ASYNC_PRIMARY_KEY = "asyncdata";
    private static final String SYNC_PRIMARY_KEY = "syncdata";

    public static void main(String[] args) {
        RedisCacheBuilder builder = new RedisCacheBuilder(new ConnectionData("127.0.0.1", 6379));
        builder.addConfigOption("timeout", "10000");

        RedisCache cache = builder.buildCache();

        cache.async().schedule().setLater(ASYNC_PRIMARY_KEY, "server", "server-1", 5);

        cache.sync().set(SYNC_PRIMARY_KEY, "startuptime", System.currentTimeMillis());

        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

        exec.schedule(() -> {
            Long time = cache.sync().get(SYNC_PRIMARY_KEY, "startuptime", Long.class);

            System.out.println("Startuptime = " + time);

            cache.async().get(ASYNC_PRIMARY_KEY, "server", String.class, (result) -> System.out.println("Server = " + result));
        }, 5, TimeUnit.SECONDS);
    }

}
